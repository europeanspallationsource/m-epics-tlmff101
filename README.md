# Thorlabs Motorized Filter Flip Mount MFF101

More info at https://www.thorlabs.com/thorproduct.cfm?partnumber=mff101.

# Prerequisites

Before IOC can be started the _/dev/bus/usb/xxx/yyy_ needs to have proper permissions; for this the udev rules file _88-thorlabs-mff101.rules_ can be copied to the _/etc/udev/rules.d_ folder.
EPICS support needs _http://www.ftdichip.com/Drivers/D2XX.htm_ library and headers. These are included in the _libftd2xx_ subfolder. Currently libftd2xx.a is used to avoid deploying libftd2xx shared library on the rootfs.

# Usage

TODO

# Misc

When connected via USB a _/dev/ttyUSBx_ device node is created, which is not what we want:

    [1227151.124021] usb 4-5: new full-speed USB device number 84 using xhci_hcd
    [1227151.262384] usb 4-5: New USB device found, idVendor=0403, idProduct=faf0
    [1227151.262387] usb 4-5: New USB device strings: Mfr=1, Product=2, SerialNumber=3
    [1227151.262389] usb 4-5: Product: APT Filter Flipper
    [1227151.262390] usb 4-5: Manufacturer: Thorlabs
    [1227151.262391] usb 4-5: SerialNumber: 37861633
    [1227151.265331] ftdi_sio 4-5:1.0: FTDI USB Serial Device converter detected
    [1227151.265368] usb 4-5: Detected FT232RL
    [1227151.265599] usb 4-5: FTDI USB Serial Device converter now attached to ttyUSB0

The supplied udev rules file _88-thorlabs-mff101.rules_ should unbind the MFF101 device from the _ftdi___sio_ kernel module in order to allow use of the userspace access through _libftd2xx_ library.
Manually this can be done like so:

    # echo ttyUSB0 > /sys/bus/usb-serial/devices/ttyUSB0/driver/unbind 

Resulting dmesg output:

    [1227160.145599] ftdi_sio ttyUSB0: FTDI USB Serial Device converter now disconnected from ttyUSB0
