#!../../bin/linux-x86_64/tlmff101DemoApp

## You may have to change tlmff101DemoApp to something else
## everywhere it appears in this file

< envPaths

epicsEnvSet("PREFIX",   "MFF101")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/tlmff101DemoApp.dbd"
tlmff101DemoApp_registerRecordDeviceDriver pdbbase

# tlMFF101Config(port, vendorNum, productNum, serialNumberStr, priority, flags)
tlMFF101Config("$(PREFIX)", 0x0403, 0xFAF0, "37861633")
asynSetTraceIOMask("$(PREFIX)",0,0xff)
#asynSetTraceMask("$(PREFIX)",0,0xff)

# Load record instances
dbLoadRecords("$(TLMFF101)/db/tlMFF101.template","P=$(PREFIX):,R=,PORT=$(PREFIX),ADDR=0,TIMEOUT=1")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=$(PREFIX):,R=asyn,PORT=$(PREFIX),ADDR=0,OMAX=100,IMAX=100")

# For Autosave, before iocInit is called
#set_requestfile_path(".")
#set_savefile_path("./autosave")
#set_pass0_restoreFile("auto_settings.sav")
#set_pass1_restoreFile("auto_settings.sav")

cd "${TOP}/iocBoot/${IOC}"
iocInit
