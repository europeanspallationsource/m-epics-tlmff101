/**
 * Asyn driver for the Thorlabs MFF.
 *
 * @author Hinko Kocevar
 * @date October 2015
 *
 */

#include <stdio.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsString.h>
#include <iocsh.h>
#include <epicsExport.h>
#include <epicsExit.h>

#include "tlMFF101.h"

static const char *driverName = "tlMFF";

//C Function prototypes to tie in with EPICS
static void tlDeviceTaskC(void *drvPvt);
static void exitHandler(void *drvPvt);

/** Constructor for Thorlabs driver; most parameters are simply passed to AsynPortDriver.
 * After calling the base class constructor this method creates a thread to collect the detector data,
 * and sets reasonable default values the parameters defined in this class and asynPortDriver.
 * \param[in] portName The name of the asyn port driver to be created.
 * \param[in] usbVID The USB VID (eg. 0x0403).
 * \param[in] usbPID The USB PID (eg. 0xFAF0).
 * \param[in] serialNumber The resource string that describes Thorlabs MFF instrument (eg. 37123456).
 * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
 * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
 */
TlMFF::TlMFF(const char *portName, int usbVID, int usbPID,
		const char *serialNumber, int priority, int stackSize)

:
		asynPortDriver(portName, 1, NUM_TL_DET_PARAMS,
                asynInt32Mask | asynOctetMask | asynEnumMask | asynDrvUserMask, /* Interface mask */
                asynInt32Mask | asynOctetMask | asynEnumMask,  /* Interrupt mask */
                0, /* asynFlags.  This driver does not block and it is not multi-device, so flag is 0 */
                1, /* Autoconnect */
                0, /* Default priority */
                0) /* Default stack size*/
{
	int status = asynSuccess;
	static const char *functionName = "TlMFF";
	char vidpid[TLMFF_BUFFER_SIZE] = {0};

	mPosition = 0;
	mUsbVID = usbVID;
	if (mUsbVID == 0) {
		mUsbVID = 0x0403;
	}
	mUsbPID = usbPID;
	if (mUsbPID == 0) {
		mUsbPID = 0xFAF0;
	}
	strncpy(mWantSerialNumber, serialNumber, TLMFF_BUFFER_SIZE);
	printf("MFF101 serial %s, VID 0x%04X, PID 0x%04X\n", mWantSerialNumber, mUsbVID, mUsbPID);

	memset(mSerialNumber, 0, TLMFF_BUFFER_SIZE);
	memset(mDeviceName, 0, TLMFF_BUFFER_SIZE);
	memset(mManufacturerName, 0, TLMFF_BUFFER_SIZE);
	//sprintf(vidpid, "0x%04X:0x%04X", mUsbVID, mUsbPID);

	/* Create an EPICS exit handler */
	epicsAtExit(exitHandler, this);

	status = createParam(TlMessageString, asynParamOctet, &TlMessage);
	status |= createParam(TlMnfcString, asynParamOctet, &TlMnfc);
	status |= createParam(TlProdString, asynParamOctet, &TlProd);
	status |= createParam(TlSerialString, asynParamOctet, &TlSerial);
	status |= createParam(TlVIDPIDString, asynParamOctet, &TlVIDPID);
	status |= createParam(TlFlipString, asynParamInt32, &TlFlip);
	status |= createParam(TlBlinkString, asynParamInt32, &TlBlink);

	if (status) {
		printf("%s:%s: unable to create parameters\n", driverName, functionName);
		return;
	}
	printf("TlBlink index = %d\n", TlBlink);

	// Use this to signal the device task to unblock.
	this->mDeviceEvent = epicsEventMustCreate(epicsEventEmpty);
	if (!this->mDeviceEvent) {
		printf("%s:%s epicsEventCreate failure for device event\n", driverName, functionName);
		return;
	}

	// addFTDevice() will try to find and initialize the device
	mInstr = NULL;

	/* Set some default values for parameters */

	status = setStringParam(TlMnfc, mManufacturerName);
	status |= setStringParam(TlProd, mDeviceName);
	// clear the serial value since device is not yet initialized
	status |= setStringParam(TlSerial, mDeviceName);
	status |= setStringParam(TlVIDPID, vidpid);
	status |= setIntegerParam(TlFlip, 0);
	status |= setIntegerParam(TlBlink, 0);

	callParamCallbacks();

	if (status) {
		printf("%s:%s: unable to set parameters\n", driverName, functionName);
		return;
	}

	if (stackSize == 0) {
		stackSize = epicsThreadGetStackSize(epicsThreadStackMedium);
	}

	/* for stopping thread */
	mFinish = 0;

	/* Create the thread that does USB device detection */
	status = (epicsThreadCreate("TlDeviceTask",
			epicsThreadPriorityMedium, stackSize, (EPICSTHREADFUNC) tlDeviceTaskC, this) == NULL);
	if (status) {
		printf("%s:%s: epicsThreadCreate failure for device task\n", driverName, functionName);
		return;
	}

	printf("MFF initialized OK!\n");
}

/**
 * Destructor.  Free resources and closes the ftd2xx library
 */
TlMFF::~TlMFF() {
	printf("Shutdown and freeing up memory...\n");
	this->lock();
	/* make sure thread is cleanly stopped */
	printf("Waiting for thread..\n");
	this->mFinish = 1;
	epicsEventSignal(mDeviceEvent);
	sleep(1);
	printf("Threads are down!\n");

	removeFTDevice();

	this->unlock();
}

/**
 * Add USB device.
 */
void TlMFF::addFTDevice(void) {

	static const char *functionName = "addFTDevice";
	char *pcBufLD[6];
	char cBufLD[5][TLMFF_BUFFER_SIZE];
	int	i;
	int	iNumDevs = 0;
	bool haveDevice = false;
	FT_DEVICE ftDevice;
    DWORD devid;
    char serialNumber[TLMFF_BUFFER_SIZE] = {0};
    char deviceName[TLMFF_BUFFER_SIZE] = {0};
    char vidpid[TLMFF_BUFFER_SIZE] = {0};

	try {
		printf("%s:%s: looking for MFF instruments\n", driverName,	functionName);
		for(i = 0; i < 5; i++) {
			pcBufLD[i] = cBufLD[i];
		}
		pcBufLD[5] = NULL;

		// use our VID and PID - THIS MANDATORY!!!!
		checkStatus("FT_SetVIDPID", FT_SetVIDPID(mUsbVID, mUsbPID));

		checkStatus("FT_ListDevices", FT_ListDevices(pcBufLD, &iNumDevs, FT_LIST_ALL | FT_OPEN_BY_SERIAL_NUMBER));

		for(i = 0; ( (i < 5) && (i < iNumDevs)); i++) {
			printf("Device %d Serial Number - %s\n", i, cBufLD[i]);

			if (strncmp(mWantSerialNumber, cBufLD[i], 8) != 0) {
				printf("Skipping the device, want serial number '%s'..\n", mSerialNumber);
				continue;
			}

			// This can fail if the ftdi_sio driver is loaded
			// use lsmod to check this and rmmod ftdi_sio to remove
			// also rmmod usbserial
			checkStatus("FT_OpenEx", FT_OpenEx(cBufLD[i], FT_OPEN_BY_SERIAL_NUMBER, &mInstr));
			printf("Opened MFF device with serial '%s'\n", cBufLD[i]);

			// get more info from the device
			checkStatus("FT_GetDeviceInfo", FT_GetDeviceInfo(mInstr, &ftDevice, &devid, serialNumber, deviceName, NULL));
            // deviceID contains encoded device ID
            // SerialNumber, Description contain 0-terminated strings
            printf("device VID 0x%04X, PID 0x%04X\n", (devid >> 16) & 0xFFFF, devid & 0xFFFF);
            printf("device serial '%s'\n", serialNumber);

            if (strncmp(mWantSerialNumber, serialNumber, 8) != 0) {
				printf("Serial number mismatch '%s' != '%s'\n", mSerialNumber, serialNumber);
				printf("Skipping the device..\n");
				checkStatus("FT_Close", FT_Close(mInstr));
				continue;
			}

            if (((devid >> 16) & 0xFFFF) != mUsbVID) {
				printf("USB VID mismatch '0x%04X' != '0x%04X'\n", mUsbVID, (devid >> 16) & 0xFFFF);
				printf("Skipping the device..\n");
				checkStatus("FT_Close", FT_Close(mInstr));
				continue;
			}

            if ((devid & 0xFFFF) != mUsbPID) {
				printf("USB PID mismatch '0x%04X' != '0x%04X'\n", mUsbPID, devid & 0xFFFF);
				printf("Skipping the device..\n");
				checkStatus("FT_Close", FT_Close(mInstr));
				continue;
			}

            strncpy(mSerialNumber, serialNumber, strlen(serialNumber));
            strncpy(mDeviceName, deviceName, strlen(deviceName));
            sprintf(mManufacturerName, "Thorlabs");
        	sprintf(vidpid, "0x%04X:0x%04X", mUsbVID, mUsbPID);

			// From APT_Communications_Protocol_Rev_15.pdf
			// Set baud rate to 115200.
			checkStatus("FT_SetBaudRate", FT_SetBaudRate(mInstr, 115200));
			// 8 data bits, 1 stop bit, no parity
			checkStatus("FT_SetDataCharacteristics", FT_SetDataCharacteristics(mInstr, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_NONE));
			// Pre purge dwell 50ms.
			usleep(50000);
			// Purge the device.
			checkStatus("FT_Purge", FT_Purge(mInstr, FT_PURGE_RX | FT_PURGE_TX));
			// Post purge dwell 50ms.
			usleep(50000);
			// Reset device.
			checkStatus("FT_ResetDevice", FT_ResetDevice(mInstr));
			// Set flow control to RTS/CTS.
			checkStatus("FT_SetFlowControl", FT_SetFlowControl(mInstr, FT_FLOW_RTS_CTS, 0, 0));
			// Set RTS.
			checkStatus("FT_SetRts", FT_SetRts(mInstr));

			haveDevice = true;
			// Device should be ready to receive commands and provide responses
		}

		if (! haveDevice) {
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: did not find device with serial '%s'\n", driverName,
					functionName, mSerialNumber);
			return;
		}
	} catch (const std::string &e) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
				functionName, e.c_str());
		return;
	}

	setStringParam(TlMnfc, mManufacturerName);
	setStringParam(TlProd, mDeviceName);
	setStringParam(TlSerial, mSerialNumber);
	setStringParam(TlVIDPID, vidpid);
	setStringParam(TlMessage, "USB device is attached!");
	callParamCallbacks();

	asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER,
			"%s:%s: filter flipper '%s' successfully initialized\n",
			driverName, functionName, mSerialNumber);
}

/**
 * Remove USB device.
 */
void TlMFF::removeFTDevice(void) {
	static const char *functionName = "removeFTDevice";

	try {
		/* Close instrument handle if opened */
		printf("Filter flipper shutting down as part of IOC exit.\n");
		if (mInstr != NULL) {
			checkStatus("FT_Close", FT_Close(mInstr));
			mInstr = NULL;
		}
	} catch (const std::string &e) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n", driverName,
				functionName, e.c_str());
		return;
	}

	setStringParam(TlMessage, "USB device is detached!");
	callParamCallbacks();

	asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER,
			"%s:%s: Filter flipper successfully shutdown\n", driverName, functionName);
}

/**
 * Exit handler, delete the TLMFF object.
 */
static void exitHandler(void *drvPvt) {
	TlMFF *pTLMFF = (TlMFF *) drvPvt;
	delete pTLMFF;
}

/** Report status of the driver.
 * Prints details about the detector in us if details>0.
 * It then calls the asynPortDriver::report() method.
 * \param[in] fp File pointed passed by caller where the output is written to.
 * \param[in] details Controls the level of detail in the report. */
void TlMFF::report(FILE *fp, int details) {
	static const char *functionName = "report";

	fprintf(fp, "Thorlabs MFF port=%s\n", this->portName);
	if (details > 0) {
		try {
			fprintf(fp, "  Manufacturer: %s\n", mManufacturerName);
			fprintf(fp, "  Model: %s\n", mDeviceName);
			fprintf(fp, "  Serial number: %s\n", mSerialNumber);
			fprintf(fp, "  VID: 0x%04X\n", mUsbVID);
			fprintf(fp, "  PID: 0x%04X\n", mUsbPID);

		} catch (const std::string &e) {
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n",
					driverName, functionName, e.c_str());
		}
	}
	// Call the base class method
	asynPortDriver::report(fp, details);
}

/** Called when asyn clients call pasynInt32->write().
 * This function performs actions for some parameters, including TlFLip, etc.
 * For all parameters it sets the value in the parameter library and calls any registered callbacks..
 * \param[in] pasynUser pasynUser structure that encodes the reason and address.
 * \param[in] value Value to write. */
asynStatus TlMFF::writeInt32(asynUser *pasynUser, epicsInt32 value) {
	int function = pasynUser->reason;

	asynStatus status = asynSuccess;
	static const char *functionName = "writeInt32";

	//Set in param lib so the user sees a readback straight away. Save a backup in case of errors.
	status = setIntegerParam(function, value);
	printf("%s func %d value %d\n", functionName, function, value);

	if (function == TlFlip) {
		try {
			// get current position
			status = getPosition();
			// set new position
			status = doFlip();
		} catch (const std::string &e) {
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n",
					driverName, functionName, e.c_str());
			status = asynError;
		}
	} else if (function == TlBlink) {
		if (value) {
			try {
				status = doBlinkLED();
			} catch (const std::string &e) {
				asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s:%s: %s\n",
						driverName, functionName, e.c_str());
				status = asynError;
			}
		}
	}

	else {
		status = asynPortDriver::writeInt32(pasynUser, value);
	}

	if (status == asynSuccess) {
		//For a successful write, clear the error message.
		setStringParam(TlMessage, " ");
	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks();

	if (status)
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
				"%s:%s: error, status=%d function=%d, value=%d\n", driverName,
				functionName, status, function, value);
	else
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
				"%s:%s: function=%d, value=%d\n", driverName, functionName,
				function, value);
	return status;
}

/**
 * Function to check the return status of Thorlabs SDK library functions.
 * @param returnStatus The return status of the SDK function
 * @return 0=success. Does not return in case of failure.
 * @throw std::string An exception is thrown in case of failure.
 */
unsigned int TlMFF::checkStatus(const char *fnc, FT_STATUS returnStatus) {
	char ebuf[256];

	if (returnStatus == FT_OK) {
		return 0;
	} else {
		// Print error
		memset(ebuf, 0, sizeof(ebuf));
		sprintf(ebuf, "%s() failed with %d", fnc, (int)returnStatus);
		// Set the error for user to see
		setStringParam(TlMessage, ebuf);

		// Close the device if it was removed (USB cable unplugged)
		if ((returnStatus == FT_IO_ERROR) || (returnStatus == FT_INSUFFICIENT_RESOURCES)) {
			removeFTDevice();
		}

		throw std::string(ebuf);
	}

	return 0;
}

void TlMFF::deviceTask(void) {
	static const char *functionName = "deviceTask";

	printf("Entered the task..\n");
	epicsEventWaitWithTimeout(mDeviceEvent, 1.0);
	printf("Running the task..\n");

	while (1) {

		if (this->mFinish) {
			asynPrint(pasynUserSelf, ASYN_TRACE_FLOW,
					"%s:%s: Stopping thread!\n", driverName, functionName);
			break;
		}

		this->lock();

		// Try to add USB device if not yet present
		if (mInstr == NULL) {
			printf("Adding the device..\n");
			addFTDevice();
		}

		this->unlock();

		epicsEventWaitWithTimeout(mDeviceEvent, 2.0);
	}

	printf("Device thread is down!\n");
}

void TlMFF::dumpBuffer(unsigned char *buffer, int elements) {
	int j;
	printf(" [");
	for (j = 0; j < elements; j++) {
		if (j > 0)
			printf(", ");
		printf("0x%02X", (unsigned int)buffer[j]);
	}
	printf("]\n");
}


asynStatus TlMFF::doBlinkLED() {
	asynStatus status = asynSuccess;

	// blink the LED on the flipper
	unsigned int wrSz;
    unsigned char wrBuf[6];
    unsigned int bufLen = 6;

    if (! mInstr) {
    	return asynDisconnected;
    }

    /* issue a MGMSG_MOD_IDENTITY command to the MFF101 */
    wrBuf[0] = 0x23;
    wrBuf[1] = 0x02;
    wrBuf[2] = 0x00;
    wrBuf[3] = 0x00;
    wrBuf[4] = 0x50;
    wrBuf[5] = 0x01;

	/* Write */
    dumpBuffer(wrBuf, bufLen);
	checkStatus("FT_Write", FT_Write(mInstr, wrBuf, bufLen, &wrSz));
	if (wrSz != bufLen) {
		printf("FT_Write only wrote %d (of %d) bytes\n", (int)wrSz, bufLen);
		status = asynError;
	}

	return status;
}

asynStatus TlMFF::getPosition() {
	asynStatus status = asynSuccess;

	// flip the position on the flipper
	unsigned int wrSz = 0;
	unsigned int rdSz = 0;
	unsigned int rdSzIn = 0;
    unsigned char wrBuf[6];
    unsigned char rdBuf[12];
    unsigned int bufLen = 6;
    unsigned int tries = 10;

    if (! mInstr) {
    	return asynDisconnected;
    }

    /* issue a MGMSG_MOT_REQ_STATUSBITS command to the MFF101 */
    wrBuf[0] = 0x29;
    wrBuf[1] = 0x04;
    wrBuf[2] = 0x01;
    wrBuf[3] = 0x00;
    wrBuf[4] = 0x50;
    wrBuf[5] = 0x01;

	/* Write */
    dumpBuffer(wrBuf, bufLen);
	checkStatus("FT_Write", FT_Write(mInstr, wrBuf, bufLen, &wrSz));
	if (wrSz != bufLen) {
		printf("FT_Write only wrote %d (of %d) bytes\n", (int)wrSz, bufLen);
		status = asynError;
	}
	bufLen = 12;
	memset(rdBuf, 0xFF, bufLen);
    while (rdSz < bufLen) {
		checkStatus("FT_GetQueueStatus", FT_GetQueueStatus(mInstr, &rdSz));
		printf("FT_GetQueueStatus() partial return %d\n", rdSz);
		usleep(50000);
		if (tries-- == 0) {
			printf("FT_GetQueueStatus() failed to return %d\n", bufLen);
    		status = asynError;
    	    return status;
		}
    }
	printf("FT_GetQueueStatus() final return %d\n", rdSz);

	checkStatus("FT_Read", FT_Read(mInstr, rdBuf, rdSz, &rdSzIn));
    dumpBuffer(rdBuf, rdSzIn);
    if (rdSzIn != rdSz) {
            printf("FT_Read only read %d (of %d) bytes\n", (int)rdSzIn, (int)rdSz);
    		status = asynError;
    }

    // 1 == forward limit switch is enabled (vertical position)
    // 2 == reverse limit switch is enabled (horizontal position)
    // the flip value needs to be just opposite of the position value!
    mPosition = rdBuf[8];

    return status;
}

asynStatus TlMFF::doFlip() {
	asynStatus status = asynSuccess;

	// flip the position on the flipper
	unsigned int wrSz = 0;
//	unsigned int rdSz = 0;
//	unsigned int rdSzIn = 0;
    unsigned char wrBuf[6];
//    unsigned char rdBuf[20];
    unsigned int bufLen = 6;

    if (! mInstr) {
    	return asynDisconnected;
    }

    // no need to issue MGMSG_MOD_REQ_CHANENABLESTATE

#if 0
    /* issue a MGMSG_MOD_REQ_CHANENABLESTATE command to the MFF101 */
    wrBuf[0] = 0x11;
    wrBuf[1] = 0x02;
    wrBuf[2] = 0x01;
    wrBuf[3] = 0x00;
    wrBuf[4] = 0x50;
    wrBuf[5] = 0x01;

	/* Write */
    dumpBuffer(wrBuf, bufLen);
	checkStatus("FT_Write", FT_Write(mInstr, wrBuf, bufLen, &wrSz));
	if (wrSz != bufLen) {
		printf("FT_Write only wrote %d (of %d) bytes\n", (int)wrSz, bufLen);
		status = asynError;
	}
	memset(rdBuf, 0xFF, bufLen);
    while (rdSz < bufLen) {
		checkStatus("FT_GetQueueStatus", FT_GetQueueStatus(mInstr, &rdSz));
    }
	printf("FT_GetQueueStatus() returned %d\n", rdSz);
	checkStatus("FT_Read", FT_Read(mInstr, rdBuf, rdSz, &rdSzIn));
    dumpBuffer(rdBuf, rdSzIn);
    if (rdSzIn != rdSz) {
            printf("FT_Read only read %d (of %d) bytes\n", (int)rdSzIn, (int)rdSz);
    		status = asynError;
    }
#endif

    /* issue a MGMSG_MOT_MOVE_JOG command to the MFF101 */
    char position = 0;
    if (mPosition == MFF101_VERTICAL) {
    	position = MFF101_HORIZONTAL;
    } else if (mPosition == MFF101_HORIZONTAL) {
    	position = MFF101_VERTICAL;
    }
    wrBuf[0] = 0x6A;
    wrBuf[1] = 0x04;
    wrBuf[2] = 0x01;
    wrBuf[3] = position;
    wrBuf[4] = 0x50;
    wrBuf[5] = 0x01;

    printf("going to position %d\n", position);

	/* Write */
    dumpBuffer(wrBuf, bufLen);
	checkStatus("FT_Write", FT_Write(mInstr, wrBuf, bufLen, &wrSz));
	if (wrSz != bufLen) {
		printf("FT_Write only wrote %d (of %d) bytes\n", (int)wrSz, bufLen);
		status = asynError;
	}

    printf("current position %d\n", position);
    status = setIntegerParam(TlFlip, position);

	return status;
}

static void tlDeviceTaskC(void *drvPvt) {
	TlMFF *pPvt = (TlMFF *) drvPvt;

	pPvt->deviceTask();
}

/** IOC shell configuration command for Thorlabs driver
 * \param[in] portName The name of the asyn port driver to be created.
 * \param[in] usbVID The USB VID (eg. 0x0403).
 * \param[in] usbPID The USB PID (eg. 0xFAF0).
 * \param[in] serialNumber The resource string that describes Thorlabs MFF instrument (eg. 37123456).
 * \param[in] priority The thread priority for the asyn port driver thread
 * \param[in] stackSize The stack size for the asyn port driver thread
 */
extern "C" {
int tlMFF101Config(const char *portName, int usbVID, int usbPID,
		const char *serialNumber, int priority, int stackSize) {
	/*Instantiate class.*/
	new TlMFF(portName, usbVID, usbPID, serialNumber, priority, stackSize);
	return (asynSuccess);
}

/* Code for iocsh registration */

/* tlMFFConfig */
static const iocshArg tlMFF101ConfigArg0 = { "Port name", iocshArgString };
static const iocshArg tlMFF101ConfigArg1 = { "USB VID", iocshArgInt };
static const iocshArg tlMFF101ConfigArg2 = { "USB PID", iocshArgInt };
static const iocshArg tlMFF101ConfigArg3 = { "Serial Number", iocshArgString };
static const iocshArg tlMFF101ConfigArg4 = { "priority", iocshArgInt };
static const iocshArg tlMFF101ConfigArg5 = { "stackSize", iocshArgInt };
static const iocshArg * const tlMFF101ConfigArgs[] = {
		&tlMFF101ConfigArg0,
		&tlMFF101ConfigArg1,
		&tlMFF101ConfigArg2,
		&tlMFF101ConfigArg3,
		&tlMFF101ConfigArg4,
		&tlMFF101ConfigArg5
};

static const iocshFuncDef configTLMFF101 = { "tlMFF101Config", 6, tlMFF101ConfigArgs };
static void configTLMFF101CallFunc(const iocshArgBuf *args) {
	tlMFF101Config(args[0].sval,
			args[1].ival,
			args[2].ival,
			args[3].sval,
			args[4].ival,
			args[5].ival);
}

static void tlMFF101Register(void) {

	iocshRegister(&configTLMFF101, configTLMFF101CallFunc);
}

epicsExportRegistrar(tlMFF101Register);

} /* extern "C" */
